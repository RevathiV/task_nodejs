var Model = require('../models');
module.exports.getAll = function (req, res) {
  Model.User.findAll()
    .then(function (users) {
      res.send(users);
    })
}

// Retrieve a single data with id
module.exports.findOne = (req, res) => {
  const id = req.params.id;
  Model.User.findByPk(id)
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({
          message: `Cannot find users with id=${id}.`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving users with id=" + id
      });
    });
};


module.exports.getAllWithContacts = function (req, res) {
  console.log(req.someHeader);
  Model.User.findAll({ include: Model.UserContact })
    .then(function (users) {
      res.send(users);
    })
}


// delete particular id
module.exports.delete = (req, res) => {
  const id = req.params.id;
  Model.User.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "users was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete users with id=${id}. Maybe users was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete users with id=" + id
      });
    });
};

//create 
/*module.exports.create = (req, res) => {
  // Validate request
  // if (!req.body.title) {
  //   res.status(400).send({
  //     message: "Content can not be empty!"
  //   });
  //   return;
  // }
  // Create a Tutorial
  const u = {
    id :req.body.id,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email,// ? req.body.email : false
    createdAt: new Date(),
    UpdateAt: new Date()
  };
  // Save Tutorial in the database
  Model.User.create(u)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Tutorial."
      });
    });
};*/
// Update the data

module.exports.update = (req, res) => {
  const id = req.params.id;
  Model.User.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "users was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update usres with id=${id}. Maybe users was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating users with id=" + id
      });
    });
};


