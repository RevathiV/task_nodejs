var express = require('express');
var router = express.Router();
var userController = require('../controller/usercontroller');
/*GET home page.*/
router.get('/',userController.getAll);
router.get('/withcontact', userController.getAllWithContacts);
// Retrieve a single users with id

// router.get("/:id", userController.findOne);
// router.delete("/:id", userController.delete);
router.post("/create", userController.create);
module.exports = router;