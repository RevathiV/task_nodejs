var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var indexRouter = require('./routes/index');
//var usersRouter = require('./routes/users');
var demoRouter = require('./routes/demo');
var app = express();
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


//user creater middleware
var somefunction=function(req , res,next){
    console.log('we are here in somefunction');
    // console.log(req);
    req['someHeader'] = 'SomeHeader';
    next();
}
app.use(somefunction);
app.use('/', indexRouter);
//app.use('/users', usersRouter);
app.use('/api/demo', demoRouter);
module.exports = app;