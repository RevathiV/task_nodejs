'use strict';

module.exports = {
  up :(queryInterface, Sequelize) =>{
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     return queryInterface.bulkInsert('UserContacts', [{
      UserId :1,
      contactName: 'Revathi',
      contactNumber: '9703913597',
      contactEmail:'asdghjkl@gmail.com',
      createdAt: new Date(),
      updatedAt: new Date(),
    },{
      UserId :1,
      contactName: 'someone',
      contactNumber: 'poiuytrew',
      contactEmail:'qwertyuih@gmail.com',
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});


  },

  down : (queryInterface, Sequelize) =>{
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     return  queryInterface.bulkDelete('UserContacts', null, {});
  }
};
