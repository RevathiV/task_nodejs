// 'use strict';
// const {
//   Model
// } = require('sequelize');
// module.exports = (sequelize, DataTypes) => {
//   class User extends Model {
//     /**
//      * Helper method for defining associations.
//      * This method is not a part of Sequelize lifecycle.
//      * The `models/index` file will call this method automatically.
//      */
//     static associate(models) {
//       // define association here

//     }
//   }
//   User.init({
//     firstName: DataTypes.STRING,
//     lastName: DataTypes.STRING,
//     email: DataTypes.STRING
//   }, {
//     sequelize,
//     modelName: 'User',
//   });
//   return User;
// };

'use strict';
module.exports=(sequelize,DataTypes)=>{
    const User = sequelize.define('User',{
     firstName:DataTypes.STRING,
     lastName:DataTypes.STRING,
     email:DataTypes.STRING,
     isActive:DataTypes.BOOLEAN
    },{});
    User.associate=function(models){
     // associations can be defined here
     User.hasMany(models.UserContact);
    };
  return User;
}