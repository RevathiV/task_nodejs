'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserContact extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      UserContact.belongsTo(models.User)
    }
  }
  UserContact.init({
    UserId: DataTypes.INTEGER,
    contactName: DataTypes.STRING,
    contactNumber: DataTypes.STRING,
    contactEmail: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'UserContact',
  });
  return UserContact;
};


// 'use strict';
// module.exports = (sequelize,DataTypes) =>{
//   const UserContact=sequelize.define('UserContact',{
//     UserId:DataTypes.INTEGER,
//     contactName:DataTypes.STRING,
//     contactNumber:DataTypes.STRING,
//     contactEmail:DataTypes.STRING
//   },{});
//   UserContact.associate=function(models){
//    // associations can be defined here
//    UserContact.belongsTo(models.User)
//   };
//  return UserContact;
// };


